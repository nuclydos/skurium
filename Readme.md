# Skurium

The goal of Skurium is to improve Desktop Linux security by utilizing user separation as an additional layer of defense.
It integrates unprivileged user accounts into the already existing Desktop Session
without violating the basic isolation properties of those accounts.

## Warning

This project is in early development.
Expect many breaking changes!

Do not install this project into any production system,
unless you've read and understood all the provided files
and tested your entire setup in a testing environment!
Mistakes during the installation process can make your operating system completely unusable
and in the worst case might even lead to permanent data loss.

Currently advanced experience with Desktop Linux systems is required
for both installing and using this project.
That said, feel free to try it out in a testing VM even if you are not an experienced Linux user yet.

So far the provided configuration has only been developed and tested for Fedora Silverblue 38.
Therefore some parts might require changes to work on other Distributions.

So far the documentation for this project is very incomplete.

## Goals

- Security

    Improved Security through compartmentalization is the primary goal of this project.

- Efficiency and Performance

    Since everything is still running natively,
    there should be no noticeable performance overhead
    and at most a minimal resource overhead.

- Freedom

    Your admin user account keeps unrestricted root access.
    You decide when you want to make use of unprivileged user accounts and when not.
    You also decide what you use each user account for,
    there is no predefined binding between apps and accounts or something like that.

- FOSS

    The entire project and all of it's required dependencies have to be Free and Open Source Software.

- Simplicity

    The concept is very simple:
    You can create and use user accounts to compartmentalize your apps and data.
    Once you've set up your system and got used to the workflow,
    there should not be much that can go wrong.
    Creating new user accounts does not involve complex and error prone configurations.

- Flexibility

    Inside of unprivileged user accounts you can do everything that does not require root privileges.
    If you have unprivileged user namespaces enabled
    (which is the default on most Linux distributions),
    then that even includes rootless containers,
    which are nearly as powerful as virtual machines but without the overhead.

    You can also invoke further sandboxing mechanisms from within unprivileged user accounts.
    This effectively allows you to create arbitrary tight but complex sandboxes
    inside a permissive but simple sandbox.

- Portability

    If a device can run Desktop Linux and allows sufficient customization,
    then it should also support this project.
    All required Software dependencies should be available for pretty much all Linux distributions,
    which support Desktop usage.
    (Note that this goal is currently not fully satisfied,
    because of the systemd dependency.)

## Limitations

- The desktop session's D-Bus is intentionally not shared
with unprivileged user accounts for security reasons.

    As a result certain desktop integration features like desktop notifications do not work.
    However, each user accounts has their own D-Bus, so D-Bus services,
    which do not rely on desktop integration, should work fine.

## Software Requirements

- Desktop Linux
- Wayland
- SSH Server and Client
- logind (Though another login manager with similar behaviour might work, too.)
- sudo (Though a symlink to doas might work, too.)
- systemd (This dependency is undesired and can hopefully be dropped in the future.)

## Hardware Requirements

None except for those implied by the Software Requirements.

## Installation instructions

The following instructions are just a small write-up to point you into the right direction.
Thus if you run into issues it's probably not your fault.
Feel free to ask for help.

### Copy the tree

Copy the files in the directory called `tree` into the filesystem of your OS.
Make sure to adjust the file ownership and permissions as needed.
The files in `tree/home/user/` are meant to be copied into the home directory of your admin user
(usually the user with UID 1000).
Since your admin user is most likely not called `user`, you need to copy the files into
`/home/<name-of-admin>/` instead of `/home/user/`.

### su

Add the following line to the file `/etc/pam.d/su`:
```
auth		required	pam_wheel.so use_uid
```

### Groups

Execute the following commands on your shell:

```
sudo groupadd --system wayland-socket
sudo groupadd --system pulse-socket
sudo groupadd --system pipewire-socket
```

This are the groups for controlling access to Wayland, PulseAudio and PipeWire.
Adding a user account to one of those groups will grant it access to the according socket.

### SSH key

Generate a new SSH keypair if you do not already have one:

```
ssh-keygen -t ed25519
```

Now copy the public key into the skeleton directory:

```
sudo mkdir /etc/skel/.ssh
sudo cp ~/.ssh/id_ed25519.pub /etc/skel/.ssh/authorized_keys
```

### Filesystem permissions

This part is of critical importance but is also highly dependant on your individual setup.
Making a mistake here could allow an attacker to trivially sidestep the security model of this project
and just read your private files conveniently.

You have to ensure that all of your private files are only accessible by the user accounts who need them!
In order to achieve that you have to adjust the permissions of the files and directories as needed.

If all your private files are in your homer directory,
then it should be sufficient to run `chmod 0700 ~`.
Also check that the home directories of newly created users are only accessible to themselves.
If that's not the case, then you will also have to adjust the corresponding system settings.
If you also store private files outside of your home directory
(for example on a separate partition),
then you also have to adjust the permissions of those files properly.

### Polyinstantiation

This part is optional as it causes some (partially fixable) Flatpak issues,
but it is highly recommended because it should improve the system's security significantly.
If you leave out this part, you should also leave out the files
`tree/usr/local/bin/flatpak` and `tree/usr/local/bin/sdrun` from the above instructions,
because their only purpose is to work around the Flatpak issues resulting from this functionality.
Both files also depend on `systemd-run`, but it is not yet clear,
if that workaround is even needed on systems without systemd,
since the corresponding Flatpak issue itself is related to systemd.
To know for sure this would need to be tested on systems without systemd.

On SELinux systems you first need to execute this:
`sudo setsebool -P polyinstantiation_enabled 1`
Otherwise you might not be able to log into your system anymore!

Now add the following code to the file `/etc/security/namespace.conf`
and replace `<name-of-admin>` with the name of your admin user account (same as above).
On systems without SELinux you also need to replace `level` with `user`.

```
/tmp     /tmp/tmp-inst/       	level      root,adm,<name-of-admin>
/var/tmp /var/tmp/tmp-inst/   	level      root,adm
```

### Reboot

Should be self-explanatory.

## Usage

This section currently also just contains the most basic information
and only documents a portion of the available functionality.

To get a login shell on another user account execute this command:

```
enter <username>
```

From that shell you should then be able to launch graphical applications in the usual way.
If graphical applications don't work,
maybe you forgot to add `<username>` to the group `wayland-socket`.

### X11

Sharing the X11 server is on purpose not supported for security reasons.
You can however setup a per-user instances of the Xwayland X11 server
by running `privateX` on the user account in question.
This also requires Wayland access though,
since the Xwayland server itself is a Wayland application.

You can then connect any shell under that user account to the Xwayland instances by running `privDisplay`.
After that you should be able to launch X11 applications from a connected shell in the usual way.
Though since the X11 server doesn't include a window manager,
you would most likely want to launch a window manager before any other X11 application.

### File transfer

Sometimes it is necessary to copy files from one user account to another.
For this it is recommended to use `scp`, a command line tool,
which should be included with the SSH client.

To copy the directory `/home/browsing/downloads/gaming_memes` and it's contents to `/home/gaming/memes`
run this as your admin user:

```
scp -r browsing@localhost:downloads/gaming_memes gaming@localhost:memes
```

If you just wanna copy a single file instead of a whole directory, then you can leave out the `-r` part.
If one of the user accounts is your admin account,
then you can leave out the `<name-of-admin>@localhost:` part for it.
In this case you can also use the tool `rsync` instead of `scp`.
`rsync` has much more features than `scp`.

`scp` is recommended because it automatically logs into the source and destination accounts
and spawns unprivileged processes for them.
All the work that has to be done for the file transfer on each side is then done by those unprivileged processes.
This follows the [principle of least privilege](https://en.wikipedia.org/wiki/Principle_of_least_privilege)
and is therefore more secure than just doing the whole file transfer as `root`.
This also guarantees that file ownership and labels are always set correctly.

## Things you should not do

This section contains an incomplete list of common or tempting behaviours,
which are dangerous in combination with this project.
It is highly recommended that you read this section!

### Never

Breaking any of the following rules can immediately break the isolation properties
of unprivileged user accounts and can allow trivial privilege escalations.
It is therefore highly recommended that you never break any of these rules!

- Never use `sudo --user` or `su` to switch to an unprivileged user account!

    `sudo` and `su` are extremely dirty ways of switching user accounts.
    They [leak most of the original environment into the target environment](https://github.com/systemd/systemd/issues/7451#issuecomment-346787237),
    including the original user's login session.
    The fact that they usually do not even allocate a new tty can allow for
    [trivial privilege escalations](https://www.errno.fr/TTYPushback.html)!

    Using `sudo` or `su` to switch between privileged user accounts
    (admin accounts or `root`)
    should be harmless in most cases,
    because these accounts cannot escalate privileges further
    and are not meant to be securely isolated from each other either.

- Never add an unprivileged user account to the `docker` group!

    Users in the `docker` group have full access to the rootful docker socket.
    This means they have effectively full root access to the host operating system
    and thus are not unprivileged anymore.
    If you want to use containers securely from within your unprivileged user accounts,
    then you should use rootless containers instead.
    This doesn't require your user accounts to be in any special group at all.
    Both Docker and Podman support rootless containers.

### Avoid

The following actions might be dangerous
or might reduce the overall security of your system,
but they are sometimes necessary and they are not always harmful.

- Avoid installing software onto your host operating system.

    Some software does increase the attack surface of the operating system.
    This is for example the case for setuid binaries or privileged services.
    Therefore it is best for your security to keep your host operating system rather minimal.
    You should try to install the software you need
    from within your unprivileged user accounts.
    Most software can be installed and used from within rootless containers.

    System-wide Flatpak app installations should be fine,
    because Flatpak apps are installed separately from the host operating system,
    cannot provide privileged system components
    and can only request unprivileged autostart at runtime.
    However, if you distrust the app itself,
    then it might still be advisable to avoid a system-wide installation,
    because otherwise you could accidentally start the app on a user account,
    where you don't want it to run.

    There are cases where installations onto the host operating system do make sense:
    - Certain applications like shells are rather useless inside of containers.
    - Installing (and updating) applications, which are required on most user accounts,
    for each account individually is not very practical and wastes resources.
    - Certain applications might be able to provide more security,
    if they're installed and properly integrated into the host operating system.
    For example on Fedora _libvirt_ can make use of SELinux
    for additional protection of it's virtual machines.
