if [ "$(id -u)" != 0 ] &&
	[ "$(id -u)" != 1000 ] &&
	# https://stackoverflow.com/questions/18431285/check-if-a-user-is-in-a-group
	! id --name --groups --zero |
		grep --quiet --null-data --line-regexp --fixed-strings wheel
then
	. /usr/local/lib/unprivProfile.sh
fi
