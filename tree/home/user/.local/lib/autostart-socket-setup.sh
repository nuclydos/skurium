#!/bin/sh

set -eu

wayland_socket="${XDG_RUNTIME_DIR}/${WAYLAND_DISPLAY}"

chmod 0777 "$wayland_socket"
