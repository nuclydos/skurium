#!/bin/sh

set -eu

if [ "$(id -u)" != 0 ]; then
	echo >&2 "This has to run as root!"
	exit 1
fi

if [ "$#" -ne 6 ]; then
	echo >&2 "Wrong number of arguments!"
	exit 1
fi

XDG_RUNTIME_DIR="$1"
WAYLAND_DISPLAY="$2"
username="$3"
mount_wayland="$4"
mount_pulse="$5"
mount_pipewire="$6"

set_additional_vars() {
	user_id="$(id -u "$username")"
	xdg_runtime_dir_target="/run/user/$user_id"
}

mount_wayland_socket() {
	wayland_source="$XDG_RUNTIME_DIR/$WAYLAND_DISPLAY"
	wayland_target="$xdg_runtime_dir_target/$WAYLAND_DISPLAY"

	while [ ! -d "$xdg_runtime_dir_target" ]; do
		sleep 1
	done

	if [ ! -S "$wayland_target" ]; then
		if [ ! -f "$wayland_target" ]; then
			touch "$wayland_target"
		fi
		mount --bind "$wayland_source" "$wayland_target"
	fi
}

mount_pulse_socket() {
	pulse_source="$XDG_RUNTIME_DIR/pulse/native"
	pulse_target="$xdg_runtime_dir_target/pulse/native"

	if ! grep --quiet -E "^tmpfs $pulse_target tmpfs " /proc/mounts; then
		while [ ! -S "$pulse_target" ]; do
			sleep 1
		done
		mount --bind "$pulse_source" "$pulse_target"
	fi
}

mount_pipewire_socket() {
	pipewire_source="$XDG_RUNTIME_DIR/pipewire-0"
	pipewire_target="$xdg_runtime_dir_target/pipewire-0"

	if ! grep --quiet -E "^tmpfs $pipewire_target tmpfs " /proc/mounts; then
		while [ ! -S "$pipewire_target" ]; do
			sleep 1
		done
		mount --bind "$pipewire_source" "$pipewire_target"
	fi
}

mount_sockets() {
	if [ "$mount_wayland" = 'true' ]; then
		mount_wayland_socket
	fi

	if [ "$mount_pulse" = 'true' ]; then
		mount_pulse_socket
	fi

	if [ "$mount_pipewire" = 'true' ]; then
		mount_pipewire_socket
	fi
}

main() {
	set_additional_vars
	mount_sockets
}

main &
