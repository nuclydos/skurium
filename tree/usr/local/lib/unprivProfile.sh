# Set XDG_SESSION_TYPE properly:

# Check if WAYLAND_DISPLAY is set
if [ -n "${WAYLAND_DISPLAY+x}" ] &&
	[ "$XDG_SESSION_TYPE" = "tty" ]
then
	XDG_SESSION_TYPE=wayland
fi

# privDisplay:

privDisplay() {
	uid="$(id -u)"

	if ps xo cmd | grep --quiet -E "^Xwayland :${uid}$"; then
		export DISPLAY=":$uid"
		XDG_SESSION_TYPE=x11
	else
		echo >&2 "Error: Private Xwayland instance is not running!"
		unset DISPLAY
		XDG_SESSION_TYPE=wayland
	fi
}
